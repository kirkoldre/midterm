#!/bin/bash
#
# script will output to screen list of file changes within the directory or child directories
#
# the script stores the initial directory file names in $readinfiles, and then compares with $checkfiles
#
clear

readinfiles="$(find . -type f | cut -f1 -d " " | sort)"

while true
do
        checkfiles="$(find . -type f | cut -f1 -d " " | sort)"
        newfilenames="$(comm -13 <(echo "$readinfiles") <(echo "$checkfiles"))"
        deletedfilenames="$(comm -23 <(echo "$readinfiles") <(echo "$checkfiles"))"

        initfileread="$(echo "$readinfiles" -exec md5sum {} \; | cut -d " " -f1 | sort)"
        samefilename="$(comm -12 <(echo "$readinfiles") <(echo "$checkfiles"))"

        hashedsamefilename="$(echo "$samefilename" -exec md5sum {} \; | cut -f1 -d " " | sort)"
        changedfile="$(comm -3 <(echo "$initfileread") <(echo "$hashedsamefilename"))"

        if [ -n "$newfilenames" ]; then
                echo "[+] File $newfilenames was added at $(date '+%Y-%m-%d_%H:%M')"; echo
        fi

        if [ -n "$deletedfilenames" ]; then
                echo "[-] File $deletedfilenames was deleted at $(date '+%Y-%m-%d_%H:%M')"; echo
        fi

        if [ -n "$changedfile" ];then
                echo "[*] File $changedfile was altered at $(date '+%Y-%m-%d_%H:%M')"; echo
        fi
        sleep 5
        readinfiles=$(echo "$checkfiles")
done